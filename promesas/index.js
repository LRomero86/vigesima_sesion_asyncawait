const fetch = require("node-fetch");

let promesa = new Promise((resolve, reject) => {
    fetch('http://api.github.com/users/andrew')
    .then((response) => response.json())
    .then(data => resolve(data))
    .catch((err) => reject(err))
});

promesa.then((result => {
    console.log(result);
}));

promesa.catch((error) => {
    console.log(error);
});