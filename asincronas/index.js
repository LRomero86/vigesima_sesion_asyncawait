const express = require('express');
const fetch = require("node-fetch");
const index = express();

//el mismo ejemplo con asíncrona

async function obtenerUsuario() {
    const response = await fetch('http://api.github.com/users/andrew');
    const data = await response.json();

    if(response.status === 200) {
        return data;
    } else {
        console.log(new Error("Se presentó un error"));
    }
}

//si lo llamamos así, sólo nos devolverá que está pendiente
//console.log(obtenerUsuario());

obtenerUsuario().then((data) => {
    console.log('La data es: ', data);
}).catch((error) => {
    console.log(error);
});

index.post('/peli', (req, res) => {

    console.log("alalalala");

    async function obtenerPelicula() {
        const response = await fetch('http://www.omdbapi.com/?t=angry birds&apikey=a2a4b086');
        const data = await response.json();
        if(data) {
            return data;
        } else {
            return res.render('/peli');
        }
    }
    
    obtenerPelicula().then(data => console.log(data));

});



index.listen(3000, function() {
    console.log('corriendo puerto 3000 en vehiculos');
});