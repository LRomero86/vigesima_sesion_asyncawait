function sumar(n1, n2) {
    return n1 + n2;
}

//usando array functions

let suma_arrayf = (n1, n2) => n1 + n2;
let multi_arrayf = (n1, n2) => n1 * n2;

console.log(sumar(10, 10));
console.log(suma_arrayf(10, 20));
console.log(multi_arrayf(5, 4));

//arrayfunctions solo para metros

let cuadrado = num => num * num; //funciones puras

console.log(cuadrado(5));

//arrow functions sin parámetros
let msj = () => "Hola mundo arrow functions";
console.log(msj);
console.log(msj());