/**
 * conversión de funciones anónimas
 * function(){} function(a) {} function(a, b) {}
 * 
 * se pasan a arrow
 * 
 * () => {}, a => {a}, (a, b) => {a * b}
 */



//función normal

function sumar(num1, num2) {
    return num1 + num2;
}

//usando arrowFunctions

let suma_arrayf = (num1, num2) => num1 + num2;

let multi_arrayf = (num1, num2) => num1 * num2;

console.log(sumar(2, 4));
console.log(multi_arrayf(4, 5));